(ns pekoe.view
  (:require [hiccup.page :refer [include-js include-css html5]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [config.core :refer [env]]
            [hickory.core :as hk]
            [pekoe.components :refer :all]
            [pekoe.util :refer :all]))

(def mount-target
  [:div#app.center-zone "Everybody loves Orange Pekoe~"])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]

   ;; Twitter card
   [:meta {:name "twitter:card" :content "summary"}]
   [:meta {:name "twitter:site" :content "@crtls"}]
   [:meta {:name "twitter:title" :content "Everybody loves Orange Pekoe!"}]
   [:meta {:name "twitter:description"
           :content "Declare your love to the lovely Churchill Mk. VII loader"}]
   [:meta {:name "twitter:image"
           :content "http://orange.pekoe.moe/images/stgloriana.png"}]
   [:meta {:name "twitter:image:alt"
           :conent "Logo of the St. Gloriana's Girl College"}]
   [:meta {:name "description"
           :content "Declare your love to the lovely Churchill Mk. VII loader"}]
   [:title (case (pekoe-event)
             :birthday "Happy birthday Orange Pekoe!"
             :valentine "Happy Valentine's day Orange Pekoe!"
             :none "Everybody loves Orange Pekoe")]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))])

(defn csrf-token []
  (hk/as-hiccup (hk/parse (anti-forgery-field))))

(defn body []
  [:div
   (main-title)
   (pekoe-img)])

(def footer
  [:footer
   [:span "By " [:a {:href "https://twitter.com/crtls"} "Zoopditto"]]])

(defn main-page []
  (html5
    (head)
    (csrf-token)
    [:body {:class "body-container"}
     (body)
     mount-target
     footer
     (include-js "/js/app.js")]))

