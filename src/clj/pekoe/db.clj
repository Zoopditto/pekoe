(ns pekoe.db
  (:require [clojure.core.async :refer [go thread chan <! <!! >! >!!]]
            [clj-http.client :as http]
            [cheshire.core :refer :all]
            [taoensso.carmine :as car :refer [wcar]]
            [pekoe.util :refer :all]))

(def geoip-url "http://freegeoip.net")
(def db-name "db")

(def server-conn {:pool {} :spec (if-let [uri (System/getenv "REDIS_URL")]
                                   {:uri uri}
                                   {:host "127.0.0.1" :port 6379})})
(defmacro wcar* [& body] `(car/wcar server-conn ~@body))

(defn entries []
  (->> (wcar* (car/hgetall* db-name))))

(defn get-location [ip]
  (let [res (http/get (str geoip-url "/json/" ip))]
    (when (= (:status res) 200)
      (-> (:body res)
          (parse-string true)
          (get :country_code)))))

(defn encode-ip [ip]
  (str "__" ip))

(defn ip-present? [ip]
  (not (nil? (wcar* (car/hget db-name (encode-ip ip))))))

(defn filter-entries []
  (let [event (pekoe-event)
        selection
        (if (= event :birthday)
          (partial filter (comp :birthday? second))
          identity)]
    (->> (entries)
         selection
         (filter (comp not nil?))
         (into #{}))))

(defn likes []
  (count (filter-entries)))

(defn countries []
  (->> (filter-entries)
       (map (comp :country second))
       (filter (comp not nil?))
       (into #{})
       count))

(defn get-state
  ([]
   {:likes     (likes)
    :countries (countries)})
  ([ip]
   {:likes     (likes)
    :countries (countries)
    :already?  (ip-present? ip)}))

(defn add-like [ip]
  (when-not (ip-present? ip)
    (let [hash-ip (encode-ip ip)]
      (wcar* (car/hset db-name hash-ip
                       {:birthday? (= (pekoe-event) :birthday)
                        :valentine? (= (pekoe-event) :valentine)
                        :country   (or (get-location ip) "???")})))
    (get-state)))

