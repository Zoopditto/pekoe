(ns pekoe.server
  (:require [pekoe.handler :refer [app]]
            [config.core :refer [env]]
            [ring.adapter.jetty :refer [run-jetty]])
  (:gen-class))

(defn -main [& args]
  (let [port (Integer/parseInt (get (System/getenv) "PORT" "8080"))
        ip (get (System/getenv) "HEROKU_CLOJURE_HTTP_IP" "0.0.0.0")]
    (run-jetty app {:ip ip :port port :join? false})))
