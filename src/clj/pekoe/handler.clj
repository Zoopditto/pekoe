(ns pekoe.handler
  (:require [clojure.core.async :refer [go thread chan >! >!! <! <!!]]
            [clojure.string :as s]
            [compojure.core :refer [GET POST PUT defroutes]]
            [compojure.route :refer [not-found resources]]
            [config.core :refer [env]]
            [cheshire.core :refer :all]
            [clj-http.client :as http]
            [pekoe.middleware :refer [wrap-middleware]]
            [pekoe.db :as db]
            [pekoe.components :refer :all]
            [pekoe.util :refer :all]
            [pekoe.view :as view]))


(defn get-client-ip [req]
  (if-let [ips (get-in req [:headers "x-forwarded-for"])]
    (-> ips (s/split #", ") first)
    (:remote-addr req)))


(defn init-request [req]
  (let [c (chan)]
    (go (>! c (db/get-state (get-client-ip req))))
    {:status  200
     :headers {"Content-Type" "application/json"}
     :body    (generate-string (<!! c))}))

(defn inc-request [req]
  (let [ip (get-client-ip req)]
    (if (db/ip-present? ip)
      {:status 200}
      (let [c (chan)]
        (go (>! c (db/add-like ip)))
        {:status  201
         :headers {"Content-Type" "application/json"}
         :body    (generate-string (<!! c))}))))

(defroutes
  routes
  (GET "/" [] (view/main-page))
  (GET "/init" req (init-request req))
  (POST "/inc" req (inc-request req))
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
