(ns pekoe.components
  (:require [pekoe.util :refer [pekoe-event]]))

(defmulti main-title pekoe-event)

(defmethod main-title :none []
  [:h2.title "Everybody loves Orange Pekoe!"])

(defmethod main-title :birthday []
  [:h2.title "It's Orange Pekoe's birthday!"])

(defmethod main-title :valentine []
  [:h2.title "Happy Valentine's day Orange Pekoe!"])

(defmulti pekoe-img pekoe-event)

(defmethod pekoe-img :none []
  [:center
   ;; Original art by Akira Sasaki
   [:a {:href "http://www.pixiv.net/member_illust.php?mode=medium&illust_id=40386642"}
    [:img.pekoe {:src "/images/pekoe.png"}]]])

(defmethod pekoe-img :birthday []
  [:center
   [:a
    [:img.pekoe {:src "/images/birthday-pekoe.jpg"}]]])

(defmethod pekoe-img :valentine []
 [:center
   [:a {:href "http://www.pixiv.net/member_illust.php?mode=medium&illust_id=40386642"}
    [:img.pekoe {:src "/images/pekoe.png"}]]])

(defn stats [state]
  (let [likes (:likes @state)
        countries (:countries @state)]
    [:div.stats
     [:p (str "Already " likes " people from " countries " countries "
              (case (pekoe-event)
                :birthday "have wished Orange Pekoe a happy birthday!"
                :valentine "❤️ Orange Pekoe!"
                :none "❤️ Orange Pekoe!"))]]))