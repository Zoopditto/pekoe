(ns pekoe.requests
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.session :as session]
            [ajax.core :refer [ajax-request]]
            [ajax.edn :refer [edn-request-format edn-response-format]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [cljs.core.async :as async]))

(defn init-req []
  (http/get "/init" {:headers {"x-csrf-token" (session/get :token)}}))

(defn inc-req []
   (http/post "/inc" {:headers {"x-csrf-token" (session/get :token)}}))
