(ns pekoe.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [accountant.core :as accountant]
            [cljs.core.async :refer [<!]]
            [pekoe.components :refer [stats]]
            [pekoe.requests :as req]
            [pekoe.util :refer [pekoe-event]]))

;; -------------------------
;; State

(def app-state (atom {:likes 0
                      :countries 0
                      :like-state :not-liked
                      :lang :en-US}))

(defn update-state [state {:keys [likes countries already?]}]
  (swap! state merge {:likes      likes
                      :countries  countries
                      :like-state (if already? :liked :not-liked)}))

(defn update-state-error [state]
  (swap! state merge {:like-state :error}))

;; -------------------------
;; Views

(defn loading []
  [:div.loading-pulse "Loading..."])

(defmulti like-button #(:like-state @%))

(defmethod like-button :not-liked
  [state]
  [:button.like-button
   {:onClick (fn [_]
               (swap! state merge {:like-state :liking})
               (go (let [res (<! (req/inc-req))]
                     (if (= (:status res) 201)
                       (swap! state merge {:like-state :liked
                                           :likes      (-> res :body :likes)
                                           :countries (-> res :body
                                                          :countries)})
                       (update-state-error state)))))}
   (case (pekoe-event)
     :birthday "Happy birthday Orange Pekoe!"
     :valentine "Happy Valentine's day Orange Pekoe!"
     :none "I ❤️ Orange Pekoe")])

(defmethod like-button :liking []
  [loading])

(defmethod like-button :liked []
  [:div [:span (case (pekoe-event)
                 :birthday "Thank you for wishing Orange Pekoe a happy birthday~"
                 :valentine "Thank you for giving Orange Pekoe chocolate"
                 :none "Thank you for liking Orange Pekoe~")]])

(defmethod like-button :error []
  [:div "Oops, something went wrong!"])

(defn main-display [state]
  [:div.center-zone
   [like-button state]
   [stats state]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (let [token (.-value (.getElementById js/document "__anti-forgery-token"))]
    (session/put! :token token)
    (reagent/render [main-display app-state] (.getElementById js/document "app"))))

(defn init! []
  (go (let [res (<! (req/init-req))]
        (if (= (:status res) 200)
          (update-state app-state (merge (:body res)))
          (update-state-error app-state))
        (mount-root))))
