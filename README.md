# Pekoe

It's a simple website displaying a picture of Orange Pekoe from the [Girls und Panzer](https://en.wikipedia.org/wiki/Girls_und_Panzer) series,
and allows the user to like her.

On her birthday (July 10th), users are invited to wish her a happy birthday.

A version is currently running [here](orange.pekoe.moe), but it might not be the same as this repo.

It is coded in Clojure and Clojurescript, using [Ring](https://github.com/ring-clojure/ring) for the back-end and [Reagent](https://holmsand.github.io/reagent/) for the front end.

# License

This work is published under the WTFPL, full text in the LICENSE file.

# Copywrite

Copywrite (c) 2016 Mériadec d'Armorique
