(ns pekoe.prod
  (:require [pekoe.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
